import { GameObject } from '../objects';
import { degToRad } from './util';

export function motionAdd(gameObject: GameObject, direction: number, speed: number): void {
  gameObject.hSpeed += speed * Math.cos(degToRad(direction - 90));
  gameObject.vSpeed += speed * Math.sin(degToRad(direction - 90));
}
