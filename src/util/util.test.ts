import { chooseRandom, randomFromRange, degToRad, radToDeg, repeat } from './util';

describe('chooseRandom', () => {
  test('chooseRandom(1) should return 1', () => {
    expect(chooseRandom(1)).toBe(1);
  });

  test(`chooseRandom('a', 'b', 'c') should return 1, 2, or 3`, () => {
    expect(chooseRandom('a', 'b', 'c')).toMatch(/^[abc]{1}$/);
  });
});

describe('randomFromRange', () => {
  const numTimesToRun = 10000;

  test(`randomFromRange(1, 10) should return a number between 1 and 10 (x${numTimesToRun} times)`, () => {
    Array(10000).fill(0).forEach(() => {
      const result = randomFromRange(1, 10);
      expect(result).toBeGreaterThanOrEqual(1);
      expect(result).toBeLessThanOrEqual(10);
    });
  });

  test(`randomFromRange(-10, 1) should return a number between -10 and 1 (x${numTimesToRun} times)`, () => {
    Array(10000).fill(0).forEach(() => {
      const result = randomFromRange(-10, 1);
      expect(result).toBeGreaterThanOrEqual(-10);
      expect(result).toBeLessThanOrEqual(1);
    });
  });
});

const degRadCases = [
  [0, 0],
  [45, Math.PI / 4],
  [90, Math.PI / 2],
  [135, Math.PI * 3 / 4],
  [180, Math.PI],
  [225, Math.PI * 5 / 4],
  [270, Math.PI * 3 / 2],
  [315, Math.PI * 7 / 4],
  [360, Math.PI * 2],
];

describe.each(degRadCases)('radToDeg', (a, b) => {
  test(`radToDeg(${b}) = ${a}`, () => {
    expect(radToDeg(b)).toBe(a);
  });
});

describe.each(degRadCases)('degToRad', (a, b) => {
  test(`degToRad(${a}) = ${b}`, () => {
    expect(degToRad(a)).toBe(b);
  });
});

describe('repeat', () => {
  test('repeat(10, fn) should result in fn being called 10 times', () => {
    const fn = jest.fn();

    repeat(10, fn);

    expect(fn).toBeCalledTimes(10);
  });

  test('repeat(-1, fn) should result in fn being called 0 times', () => {
    const fn = jest.fn();

    repeat(-1, fn);

    expect(fn).toBeCalledTimes(0);
  });
});
