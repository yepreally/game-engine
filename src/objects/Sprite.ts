import { degToRad } from '../util';

export interface ISpriteOptions {
  height: number;
  width: number;
}

export class Sprite {
  image: HTMLImageElement;
  protected isReady: boolean;

  height: number;
  width: number;

  constructor(imageSrc: string, options?: Partial<ISpriteOptions>) {
    this.height = options?.height ?? 16;
    this.width = options?.width ?? 16;
    // TODO add support for mount location. Center-only support at the moment.

    this.isReady = false;
    this.image = this.createImage(imageSrc);
  }

  protected createImage(imageSrc: string): HTMLImageElement {
    const image = new Image();
    image.onload = () => {
      this.isReady = true;
    };
    image.src = imageSrc;
    return image;
  }

  render(canvas: HTMLCanvasElement, x: number, y: number, rotation: number, opacity: number): void {
    const ctx = canvas.getContext('2d');
    if (this.isReady && ctx) {
      ctx.save();
      ctx.translate(x, y);
      ctx.rotate(degToRad(rotation));
      ctx.globalAlpha = opacity;
      ctx.drawImage(this.image, -this.width / 2, -this.height / 2, this.width, this.height);
      ctx.restore();
    }
  }
}
