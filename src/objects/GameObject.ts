import { EventEmitter, Game, IListener, IUnsubscribe, Screen } from '../base';
import { degToRad, radToDeg } from '../util';

export type ICollisionListener<T = unknown, G extends Game = Game > = IListener<{ object: T, game: G, screen: Screen }>;

export interface IGameObjectOptions {
  x: number;
  y: number;
  height: number;
  width: number;
  rotation: number;
  opacity: number;
  screenWrap: boolean;
  direction: number;
  speed: number;
}

type IOnCreatePayload = object;

interface IOnAddPayload<G extends Game = Game> {
  screen: Screen;
  game: G;
}

interface IOnUpdatePayload<G extends Game = Game> {
  screen: Screen;
  game: G;
}

interface IOnRemovePayload<G extends Game = Game> {
  screen: Screen;
  game: G;
}

export abstract class GameObject<G extends Game = Game> {
  readonly type: string;
  protected eventEmitter: EventEmitter;
  protected collisionListeners: { type: string, listeners: ICollisionListener[] }[];

  x: number;
  y: number;
  rotation: number;
  opacity: number;
  screenWrap: boolean;

  protected _height: number;
  protected _width: number;

  protected _direction: number;
  protected _speed: number;
  protected _hSpeed: number;
  protected _vSpeed: number;

  constructor(type: string, options?: Partial<IGameObjectOptions>) {
    this.type = type;
    this.eventEmitter = new EventEmitter();
    this.collisionListeners = [];

    this.x = options?.x ?? 0;
    this.y = options?.y ?? 0;
    this.rotation = options?.rotation ?? 0;
    this.opacity = options?.opacity ?? 1;
    this.screenWrap = options?.screenWrap ?? false;

    this._height = options?.height ?? 16;
    this._width = options?.width ?? 16;

    // calling the direction and speed setters will initialize _direction, _speed, _hSpeed, and _vSpeed
    this._direction = options?.direction ?? 0;
    this._speed = options?.speed ?? 0;
    this._hSpeed = this.computeHSpeed();
    this._vSpeed = this.computeVSpeed();

    this.addInteractions();
    this.triggerCreate();
  }

  protected computeHSpeed(): number {
    return this._speed * Math.cos(degToRad(this._direction - 90));
  }

  protected computeVSpeed(): number {
    return this._speed * Math.sin(degToRad(this._direction - 90));
  }

  get speed(): number {
    return this._speed;
  }

  set speed(speed: number) {
    this._speed = speed;
    this._hSpeed = this.computeHSpeed();
    this._vSpeed = this.computeVSpeed();
  }

  get direction(): number {
    return this._direction;
  }

  set direction(direction: number) {
    this._direction = direction;
    this._hSpeed = this.computeHSpeed();
    this._vSpeed = this.computeVSpeed();
  }

  get vSpeed(): number {
    return this._vSpeed;
  }

  set vSpeed(vSpeed: number) {
    this._vSpeed = vSpeed;
    this._speed = Math.sqrt(this._vSpeed * this._vSpeed + this._hSpeed * this._hSpeed);
    this._direction = radToDeg(Math.atan2(this._vSpeed, this._hSpeed)) + 90;
  }

  get hSpeed(): number {
    return this._hSpeed;
  }

  set hSpeed(hSpeed: number) {
    this._hSpeed = hSpeed;
    this._speed = Math.sqrt(this._vSpeed * this._vSpeed + this._hSpeed * this._hSpeed);
    this._direction = radToDeg(Math.atan2(this._vSpeed, this._hSpeed)) + 90;
  }

  get width(): number {
    return this._width;
  }

  get height(): number {
    return this._height;
  }

  abstract addInteractions(): void;

  triggerCreate(): void {
    this.eventEmitter.emit('create', {});
  }

  protected onCreate(listener: IListener<IOnCreatePayload>): IUnsubscribe {
    return this.eventEmitter.on('create', listener);
  }

  triggerAdd(payload: IOnAddPayload): void {
    this.eventEmitter.emit('add', payload);
  }

  protected onAdd(listener: IListener<IOnAddPayload<G>>): IUnsubscribe {
    return this.eventEmitter.on('add', listener);
  }

  triggerUpdate(payload: IOnUpdatePayload): void {
    this.eventEmitter.emit('update', payload);
  }

  protected onUpdate(listener: IListener<IOnUpdatePayload<G>>): IUnsubscribe {
    return this.eventEmitter.on('update', listener);
  }

  triggerRemove(payload: IOnRemovePayload): void {
    this.eventEmitter.emit('remove', payload);
  }

  protected onRemove(listener: IListener<IOnRemovePayload<G>>): IUnsubscribe {
    return this.eventEmitter.on('remove', listener);
  }

  update(screen: Screen, game: G): void {
    this.triggerUpdate({ screen, game });

    if (this._speed === 0) {
      return;
    }

    this.x = this.x + this._hSpeed;
    this.y = this.y + this._vSpeed;

    if (this.screenWrap) {
      const halfWidth = this.width / 2;
      const halfHeight = this.height / 2;

      const gameWidth = game.width;
      if (this.x - halfWidth > gameWidth) {
        this.x = -halfWidth;
      } else if (this.x + halfWidth < 0) {
        this.x = gameWidth + halfWidth;
      }

      const gameHeight = game.height;
      if (this.y - halfHeight > gameHeight) {
        this.y = -halfHeight;
      } else if (this.y + halfHeight < 0) {
        this.y = gameHeight + halfHeight;
      }
    }
  }

  abstract render(canvas: HTMLCanvasElement): void;

  // triggerCollision<T>(type: string, payload: { object: T, game: Game, screen: Screen }): void {
  //   this.eventEmitter.emit(`collision-${type}`, payload);
  // }

  protected addCollisionListener(type: string, listenerToAdd: ICollisionListener): void {
    const typedListenersIdx = this.collisionListeners.findIndex(cl => cl.type === type);
    if (typedListenersIdx === -1) {
      this.collisionListeners.push({ type, listeners: [listenerToAdd] });
    } else {
      const newCollisionListeners = [...this.collisionListeners];
      newCollisionListeners[typedListenersIdx].listeners.push(listenerToAdd);
      this.collisionListeners = newCollisionListeners;
    }
  }

  protected removeCollisionListener(type: string, listenerToRemove: ICollisionListener): void {
    const typedListenersIdx = this.collisionListeners.findIndex(cl => cl.type === type);
    if (typedListenersIdx === -1) {
      // GameObject does not have any listeners of this type
      return;
    }

    const typedListeners = this.collisionListeners[typedListenersIdx];
    const newListeners = typedListeners.listeners.filter(l => l !== listenerToRemove);
    if (newListeners.length === typedListeners.listeners.length) {
      // GameObject does not have this listener
      return;
    }

    if (newListeners.length === 0) {
      this.collisionListeners = this.collisionListeners.filter(cl => cl.type !== type);
    } else {
      const newCollisionListeners = [...this.collisionListeners];
      newCollisionListeners[typedListenersIdx].listeners = newListeners;
      this.collisionListeners = newCollisionListeners;
    }
  }

  onCollisionWith<T>(type: string, listener: ICollisionListener<T, G>): IUnsubscribe {
    this.addCollisionListener(type, listener as ICollisionListener);

    return () => {
      this.removeCollisionListener(type, listener as ICollisionListener);
    };
  }

  protected isCollidingWith(object: GameObject): boolean {
    const dx = this.x - object.x;
    const dy = this.y - object.y;
    const distance = Math.sqrt(dx * dx + dy * dy);
    return distance < Math.max(this.height, this.width) / 2 + Math.max(object.height, object.width) / 2;
  }

  checkCollisions(screen: Screen, game: G, objectsByType: { [key: string]: GameObject[] }): void {
    this.collisionListeners.forEach(({ type, listeners }) => {
      const gameObjects = objectsByType[type];
      if (gameObjects?.length > 0) {
        gameObjects.forEach(gameObject => {
          if (this.isCollidingWith(gameObject)) {
            const payload = { object: gameObject, game, screen };
            listeners.forEach(listener => listener(payload));
          }
        });
      }
    });
  }
}
