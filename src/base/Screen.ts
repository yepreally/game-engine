import { GameObject } from '../objects';
import { EventEmitter, IListener, IUnsubscribe } from './EventEmitter';
import { Game } from './Game';

interface IOnLoadPayload<T extends Game = Game> {
  game: T;
}

interface IOnUnloadPayload<T extends Game = Game> {
  game: T;
}

export abstract class Screen<T extends Game = Game> {
  protected eventEmitter: EventEmitter;
  protected gameObjects: GameObject[];
  protected gameObjectsByType: { [key: string]: GameObject[] };

  protected game: T | null;

  constructor() {
    this.eventEmitter = new EventEmitter();
    this.game = null;
    this.gameObjects = [];
    this.gameObjectsByType = {};
    this.addInteractions();
  }

  abstract addInteractions(): void;

  triggerLoad(payload: IOnLoadPayload<T>): void {
    this.eventEmitter.emit('load', payload);
  }

  protected onLoad(listener: IListener<IOnLoadPayload<T>>): IUnsubscribe {
    return this.eventEmitter.on('load', listener);
  }

  triggerUnload(payload: IOnUnloadPayload<T>): void {
    this.eventEmitter.emit('unload', payload);
  }

  protected onUnload(listener: IListener<IOnUnloadPayload<T>>): IUnsubscribe {
    return this.eventEmitter.on('unload', listener);
  }

  get width(): number {
    return this.game?.width ?? 0;
  }

  get height(): number {
    return this.game?.height ?? 0;
  }

  load(game: T): void {
    this.game = game;
    this.triggerLoad({ game: this.game });
  }

  unload(game: T): void {
    this.triggerUnload({ game });
  }

  update(): void {
    if (!this.game) {
      return;
    }

    for (let j = 0; j < this.gameObjects.length; j++) {
      this.gameObjects[j].update(this, this.game);
    }
    for (let j = 0; j < this.gameObjects.length; j++) {
      this.gameObjects[j].checkCollisions(this, this.game, this.gameObjectsByType);
    }
  }

  render(canvas: HTMLCanvasElement): void {
    for (let j = 0; j < this.gameObjects.length; j++) {
      this.gameObjects[j].render(canvas);
    }
  }

  addObject(gameObject: GameObject): Screen {
    if (!this.game) {
      return this;
    }

    if (!this.gameObjects.includes(gameObject)) {
      this.gameObjects.push(gameObject);
      if (!this.gameObjectsByType[gameObject.type]) {
        this.gameObjectsByType[gameObject.type] = [gameObject];
      } else {
        this.gameObjectsByType[gameObject.type].push(gameObject);
      }
      gameObject.triggerAdd({ screen: this, game: this.game });
    }
    return this;
  }

  removeObject(gameObject: GameObject): Screen {
    if (!this.game) {
      return this;
    }

    if (this.gameObjects.includes(gameObject)) {
      this.gameObjects = this.gameObjects.filter(g => g !== gameObject);
      if (this.gameObjectsByType[gameObject.type].length > 1) {
        this.gameObjectsByType[gameObject.type] = this.gameObjectsByType[gameObject.type].filter(g => g !== gameObject);
      } else {
        delete this.gameObjectsByType[gameObject.type];
      }
      gameObject.triggerRemove({ screen: this, game: this.game });
    }
    return this;
  }

  getAllObjects(): GameObject[] {
    return this.gameObjects;
  }

  getObjectsByType(type: string): GameObject[] {
    return this.gameObjectsByType[type] ?? [];
  }
}
